# Nom du compilateur
CC = gcc

# Options de compilation
CFLAGS  = -Wall -Wextra -Werror -g
LDFLAGS = -lm

# Nom de l'exécutable
TARGET = bin/main

# Liste des fichiers source
SRCS = src/main.c src/functions.c src/simulations.c src/lib/mt19937ar.c

# Liste des fichiers objets
OBJS = $(patsubst %.c,bin/%.o,$(SRCS))

# Règle par défaut
all: $(TARGET)

# Règle pour créer l'exécutable
$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) -o $(TARGET) $(OBJS) $(LDFLAGS)

# Règle pour créer les fichiers objets
bin/%.o: %.c
	@mkdir -p $(dir $@)
	$(CC) $(CFLAGS) -c $< -o $@

# Règle pour nettoyer les fichiers générés
clean:
	rm -f bin/*.o bin/src/*.o bin/src/lib/*.o $(TARGET)

.PHONY: all clean
