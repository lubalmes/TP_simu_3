#ifndef SIMULATIONS_H
#define SIMULATIONS_H

double sim_pi(unsigned long nb_points);
double* sim_multiple_pi(unsigned long nb_points, unsigned long nb_simulations);

#endif // SIMULATIONS_H