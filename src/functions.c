/**
 * @file functions.c
 * @brief Fonctions de simulation
 *
 * Ce fichier contient les fonctions mathématiques pour la simulation.
 *
 * @author Lucas BALMÈS
 * @date 2024-09-23
 */

#include <stdlib.h>
#include <math.h>

#include "lib/mt19937ar.h"

#define M_PI 3.14159265358979323846

double mean(double *values, unsigned long length)
{
    double sum = 0;
    for (unsigned long i = 0; i < length; i++)
    {
        sum += values[i];
    }
    return sum / length;
}

/**
 * @brief Calcul la différence entre un nombre et Pi
 * 
 * Cette fonction calcule la différence entre un nombre et Pi.
 * 
 * @param a Le nombre à comparer à Pi
 * @return double La différence entre a et Pi
 */
double diff_pi(double a)
{
    return a - M_PI;
}

/**
 * @brief Calcul de la valeur de t
 * 
 * Cette fonction calcule la valeur de t pour un niveau de confiance de 95%.
 * (alpha = 0.05)
 * Valable pour n <= 30
 * 
 * @param n Le nombre de degrés de liberté
 * @return double La valeur de t
 */
double t_value(unsigned long n){
    n = n - 1;
    double tab[30] = {12.706, 4.303, 3.182, 2.776, 2.571, 2.447, 2.365, 2.306, 2.262, 2.228, 2.201, 2.179, 2.160, 2.145, 2.131, 2.120, 2.110, 2.101, 2.093, 2.086, 2.080, 2.074, 2.069, 2.064, 2.060, 2.056, 2.052, 2.048, 2.045, 2.042};
    return tab[n];
}

/**
 * @brief Calcul d'estimation de la variance
 * 
 * Cette fonction calcule une estimation de la variance.
 * 
 * @param values Les valeurs à utiliser pour le calcul
 * @param length La longueur des valeurs
 * @param mean La moyenne des valeurs
 * 
 * @return double L'estimation de la variance
 */
double s_square(double *values, unsigned long length, double mean)
{
    double sum = 0;
    for (unsigned long i = 0; i < length; i++)
    {
        sum += pow(values[i] - mean, 2);
    }
    return sum / length;
}

/**
 * @brief Calcul du rayon de confiance
 * 
 * Cette fonction calcule le rayon de confiance.
 * 
 * @param values Les valeurs à utiliser pour le calcul
 * @param length La longueur des valeurs
 * 
 * @return double Le rayon de confiance
 */
double confidence_radius(double* values, unsigned long length)
{
    double m_val = mean(values, length);
    double s_val = s_square(values, length, m_val);
    return t_value(length) * sqrt(s_val / length);
}
