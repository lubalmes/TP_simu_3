/**
 * @file simulations.c
 * @brief Scénarios de simulation
 * 
 * Ce fichier contient les fonctions de simulation.
 * 
 * @author Lucas BALMÈS
 * @date 2024-09-23
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lib/mt19937ar.h"
#include "functions.h"


/**
 * @brief Simulation de Pi par la méthode de Monte Carlo
 * 
 * Cette fonction simule la valeur de Pi par la méthode de Monte Carlo.
 * 
 * @param nb_points Le nombre de points à utiliser pour la simulation
 * @return double La valeur de Pi simulée
 */
double sim_pi(unsigned long nb_points){
    unsigned long nb_points_dans_cercle = 0;
    double x, y;
    for(unsigned long i = 0; i < nb_points; i++){
        x = genrand_real1();
        y = genrand_real1();
        if(x*x + y*y <= 1){
            nb_points_dans_cercle++;
        }
    }
    return 4.0 * nb_points_dans_cercle / nb_points;
}

/**
 * @brief Simulation de plusieurs valeurs de Pi par la méthode de Monte Carlo
 * 
 * @param nb_points Le nombre de points à utiliser pour la simulation
 * @param nb_simulations Le nombre de simulations à effectuer
 * 
 * @return double* Les résultats des simulations
 */
double* sim_multiple_pi(unsigned long nb_points, unsigned long nb_simulations){

    double* results;
    // Allocation de la mémoire pour les résultats
    results = (double*)malloc(nb_simulations * sizeof(double));

    for(unsigned long i = 0; i < nb_simulations; i++){
        results[i] = sim_pi(nb_points);
    }
    return results;
}
