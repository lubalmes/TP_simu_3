/**
 * @file main.c
 * @brief Fichier principal du programme du TP 2 de simulation
 *
 * Ce fichier contient la fonction principale du programme du TP de simulation 2.
 * Ainsi que la fonction d'initialisation du générateur de nombres pseudo-aléatoires Merseene Twister.
 *
 * @author Lucas BALMÈS
 * @date 2024-09-23
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "lib/mt19937ar.h"
#include "functions.h"
#include "simulations.h"

/**
 * @brief Fonction d'initialisation du générateur de nombres pseudo-aléatoires Merseene Twister.
 *
 * Cette fonction initialise le générateur de nombres pseudo-aléatoires Merseene Twister.
 *
 * @return void
 */
void init_mt19937ar()
{
    unsigned long init[4] = {0x123, 0x234, 0x345, 0x456}, length = 4;
    init_by_array(init, length);
}

void question_1()
{
    unsigned long nb_points = 10000;

    printf("Valeur de Pi simulée à %ld points: %f\n", nb_points, sim_pi(nb_points));
}

void question_2()
{
    unsigned long const nb_simulations = 30;
    unsigned long const nb_points = 1000;

    double* res;
    double mean_res = 0;

    res = sim_multiple_pi(nb_points, nb_simulations);
    mean_res = mean(res, nb_simulations);

    printf("Moyenne de Pi simulée à %ld points avec %ld simulations: %f\n", nb_points, nb_simulations, mean_res);
    printf("Différence avec Pi: %f\n", diff_pi(mean_res));
}

void question_3()
{
    unsigned long const nb_simulations = 30;
    unsigned long const nb_points = 1000000;

    double *results = NULL;
    results = sim_multiple_pi(nb_points, nb_simulations);

    printf("Moyenne de Pi simulée à %ld points avec %ld simulations: %f\n", nb_points, nb_simulations, mean(results, nb_simulations));
    printf("Rayon de confiance: %f\n", confidence_radius(results, nb_simulations));

    free(results);
}

/**
 * @brief Fonction principale du programme
 *
 * Cette fonction est la fonction principale du programme.
 *
 * @param argc Le nombre d'arguments passés au programme
 * @param argv Les arguments passés au programme
 *
 * @return int Le code de retour du programme
 */
int main()
{
    init_mt19937ar();

    // question_1();
    // question_2();
    // question_3();

    return 0;
}
