#ifndef FUNCTIONS_H
#define FUNCTIONS_H

double mean(double *values, unsigned long length);
double diff_pi(double a);
double confidence_radius(double* values, unsigned long length);

#endif // FUNCTIONS_H